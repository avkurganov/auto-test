package com.myproj;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.util.List;

public class LoginSteps {


    @Then("^I should see the userform page$")
    public void iShouldSeeTheUserformPage()  {

        System.out.println("I should see the userform page");


    }

    @Given("^I navigate to the login page$")
    public void iNavigateToTheLoginPage() {

        System.out.println("I navigate to the login page with: " );

    }

    @And("^I enter the username as admin and password as admin$")
    public void iEnterTheUsernameAsAdminAndPasswordAsAdmin()  {

        System.out.println("I enter the username as admin and password as admin");

    }

    @And("^I click login button$")
    public void iClickLoginButton() {

        System.out.println("I click login button");

    }

    @And("^I enter the username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
    public void iEnterTheUsernameAsAndPasswordAs(String arg0, String arg1)  {

        System.out.println("User Name: " + arg0 + " : " + "PASSWORD: " + arg1);

    }

    @And("^I enter the following for Login$")
    public void iEnterTheFollowingForLogin(DataTable table) {

        List<List<String>> data = table.raw();

        System.out.println(data.get(1).get(0));
        System.out.println(data.get(1).get(1));

//        List<User> users = new ArrayList<User>();


    }
}

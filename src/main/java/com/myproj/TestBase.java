package com.myproj;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import io.advantageous.boon.core.Str;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestBase extends AbstractTestNGCucumberTests {

    private WebDriver driver;

    @Parameters({"browser"})
    @BeforeClass
    public void setUp(@Optional("firefox") String browser) {

       System.out.println("This will run before the Scenario: " + browser);
       String screenshot = System.getProperty("screenshot");


       System.out.println("Need a ScreenShot? " + screenshot);

       if (browser.equals("firefox")) {

           System.setProperty("webdriver.gecko.driver", "/Users/avkurganov/Downloads/geckodriver");

           driver = new FirefoxDriver();

           driver.get("https://bcgdv.com/");

           waitTillPageHasBeenFullyLoaded();

           String pageTitle = driver.getTitle();

           File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

           DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy_HH:mm:ss");
           Date date = new Date();

           try {
               FileUtils.copyFile(scrFile, new File(dateFormat.format(date) + "_" + pageTitle +  ".png"));
           } catch (IOException e) {
               e.printStackTrace();
           }
           System.out.println("Screenshot is captured for failed testcase");


           System.out.println("Page Loaded...");

       }
    }

    public void waitTillPageHasBeenFullyLoaded() {
        new WebDriverWait(driver, 10000).until(
                webDriver -> (
                        (JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete")
        );
    }

    @AfterClass
    public void tearDown(){

       if (driver != null) {

            driver.close();
       }
    }

}

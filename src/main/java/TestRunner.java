import com.myproj.TestBase;
import cucumber.api.CucumberOptions;

@CucumberOptions(
        features = {"src/test/resources"},
        glue = {"com.myproj"},
        tags = {"@Smoke", "@Admin"},
        format = {
                "pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/CucumberTestReport.json",
                "rerun:target/cucumber-reports/rerun.txt"
        })

 public class TestRunner extends TestBase {




}
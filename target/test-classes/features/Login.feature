@Smoke
Feature: Login Feature
    This feature deals with the login functionality of the application

@Admin
  Scenario: Login with correct username and password with "browser"
    Given I navigate to the login page
    And I enter the following for Login
      | AdminName | Password     |
      | admin     | adminpassword |
    And I click login button
    Then I should see the userform page

  @User
  Scenario: Login with correct username and password
    Given I navigate to the login page
    And I enter the following for Login
      | UserName | Password      |
      | user     | userpassword |
    And I click login button
    Then I should see the userform page